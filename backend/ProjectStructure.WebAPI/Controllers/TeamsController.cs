﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ProjectStructure.BLL.DTO;
using System.Collections.Generic;
using Microsoft.AspNetCore.Http;
using ProjectStructure.BLL.Services.Interfaces;

namespace ProjectStructure.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TeamsController : ControllerBase
    {
        private readonly IService<TeamDTO> _teamService;


        public TeamsController(IService<TeamDTO> teamService)
        {
            _teamService = teamService;
        }


        [HttpGet]
        public async Task<ActionResult<IEnumerable<TeamDTO>>> Get()
        {
            return Ok(await _teamService.GetAllEntitiesAsync());
        }


        [HttpGet("{id}")]
        public async Task<ActionResult<TeamDTO>> Get(int id)
        {
            if (id <= 0)
                return BadRequest("Team ID cannot be less than or equal to zero");

            var team = await _teamService.GetEntityAsync(id);

            if (team == null)
                return NotFound("There are no team with this Id");

            return Ok(team);
        }


        [HttpPost]
        public async Task<ActionResult> Post([FromBody] TeamDTO teamDTO)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            try
            {
                var team = await _teamService.AddEntityAsync(teamDTO);
                return CreatedAtAction(nameof(Get), new { Id = team.Id }, team);
            }
            catch (ArgumentException ex)
            {
                return StatusCode(StatusCodes.Status400BadRequest, ex.Message);
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            }
        }


        [HttpPut]
        public async Task<ActionResult> Put([FromBody] TeamDTO teamDTO)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            try
            {
                var team = await _teamService.UpdateEntityAsync(teamDTO);
                return Ok(team);
            }
            catch (ArgumentException ex)
            {
                return StatusCode(StatusCodes.Status400BadRequest, ex.Message);
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            }
        }


        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(int id)
        {
            if (id <= 0)
                return BadRequest("Team ID cannot be less than or equal to zero");

            try
            {
                var team = await _teamService.GetEntityAsync(id);

                if (team == null)
                    return NotFound("There are no team with this Id");

                return Ok(await _teamService.DeleteEntityAsync(id));
            }
            catch (ArgumentNullException ex)
            {
                return StatusCode(StatusCodes.Status404NotFound, ex.Message);
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            }
        }
    }
}
