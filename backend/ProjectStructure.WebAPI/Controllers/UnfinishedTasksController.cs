﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ProjectStructure.BLL.DTO;
using Microsoft.AspNetCore.Http;
using System.Collections.Generic;
using ProjectStructure.BLL.Services;

namespace ProjectStructure.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UnfinishedTasksController : ControllerBase
    {
        private readonly UnfinishedTaskService _unfinishedTaskService;


        public UnfinishedTasksController(UnfinishedTaskService unfinishedTaskService)
        {
            _unfinishedTaskService = unfinishedTaskService;
        }


        [HttpGet("{id}")]
        public async Task<ActionResult<List<TaskDTO>>> Get(int id)
        {
            try
            {
                var tasks = await _unfinishedTaskService.GetUnfinishedTasksAsync(id);

                if (tasks == null || tasks.Count == 0)
                    return NotFound($"The performer with Id={id} doesn't have any unfinished tasks");
                else
                    return Ok(tasks);
            }
            catch (ArgumentOutOfRangeException ex)
            {
                return StatusCode(StatusCodes.Status400BadRequest, ex.Message);
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            }
        }
    }
}
