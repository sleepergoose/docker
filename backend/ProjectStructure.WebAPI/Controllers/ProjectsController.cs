﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ProjectStructure.BLL.DTO;
using Microsoft.AspNetCore.Http;
using System.Collections.Generic;
using ProjectStructure.BLL.Services.Interfaces;

namespace ProjectStructure.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProjectsController : ControllerBase
    {
        private readonly IService<ProjectDTO> _projectService;


        public ProjectsController(IService<ProjectDTO> projectService)
        {
            _projectService = projectService;
        }


        [HttpGet]
        public async Task<ActionResult<IEnumerable<ProjectDTO>>> Get()
        {
            return Ok(await _projectService.GetAllEntitiesAsync());
        }


        [HttpGet("{id}")]
        public async Task<ActionResult<ProjectDTO>> Get(int id)
        {
            if (id <= 0)
                return BadRequest("Project ID cannot be less than or equal to zero");

            var project = await _projectService.GetEntityAsync(id);

            if (project == null)
                return NotFound("There are no project with this Id");

            return Ok(project);
        }


        [HttpPost]
        public async Task<ActionResult> PostAsync([FromBody] ProjectDTO projectDTO)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            try
            {
                var project = await _projectService.AddEntityAsync(projectDTO);
                return CreatedAtAction(nameof(Get), new { Id = project.Id }, project);
            }
            catch (ArgumentException ex)
            {
                return StatusCode(StatusCodes.Status400BadRequest, ex.Message);
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            }
        }


        [HttpPut]
        public async Task<ActionResult> Put([FromBody] ProjectDTO projectDTO)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            try
            {
                var project = await _projectService.UpdateEntityAsync(projectDTO);
                return Ok(project);
            }
            catch (ArgumentException ex)
            {
                return StatusCode(StatusCodes.Status400BadRequest, ex.Message);
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            }
        }


        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(int id)
        {
            if (id <= 0)
                return BadRequest("Project ID cannot be less than or equal to zero");

            try
            {
                var project = await _projectService.GetEntityAsync(id);

                if (project == null)
                    return NotFound("There are no project with this Id");

                return Ok(await _projectService.DeleteEntityAsync(id));
            }
            catch (ArgumentNullException ex)
            {
                return StatusCode(StatusCodes.Status404NotFound, ex.Message);
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            }
        }
    }
}
