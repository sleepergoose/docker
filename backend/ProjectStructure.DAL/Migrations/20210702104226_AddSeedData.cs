﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ProjectStructure.DAL.Migrations
{
    public partial class AddSeedData : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "Teams",
                columns: new[] { "Id", "CreatedAt", "TeamName" },
                values: new object[,]
                {
                    { 1, new DateTime(2017, 12, 5, 22, 18, 49, 13, DateTimeKind.Unspecified).AddTicks(7347), "Computer" },
                    { 2, new DateTime(2019, 6, 19, 12, 46, 13, 622, DateTimeKind.Unspecified).AddTicks(1806), "Salad" },
                    { 3, new DateTime(2021, 2, 24, 10, 22, 21, 722, DateTimeKind.Unspecified).AddTicks(2472), "Soap" },
                    { 4, new DateTime(2018, 10, 27, 22, 30, 33, 117, DateTimeKind.Unspecified).AddTicks(2758), "Tuna" },
                    { 5, new DateTime(2016, 12, 10, 0, 45, 49, 148, DateTimeKind.Unspecified).AddTicks(6875), "Keyboard" }
                });

            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "Id", "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[,]
                {
                    { 4, new DateTime(1992, 12, 8, 5, 40, 31, 200, DateTimeKind.Unspecified).AddTicks(8372), "Kristina.Fadel18@yahoo.com", "Kristina", "Fadel", new DateTime(2020, 9, 16, 23, 36, 16, 972, DateTimeKind.Unspecified).AddTicks(3930), 1 },
                    { 20, new DateTime(1993, 3, 12, 9, 34, 12, 196, DateTimeKind.Unspecified).AddTicks(2074), "Becky21@hotmail.com", "Becky", "Weber", new DateTime(2020, 11, 9, 10, 28, 38, 603, DateTimeKind.Unspecified).AddTicks(4766), 5 },
                    { 15, new DateTime(1982, 8, 23, 10, 18, 2, 295, DateTimeKind.Unspecified).AddTicks(2399), "Greg.Jerde@hotmail.com", "Greg", "Jerde", new DateTime(2018, 12, 31, 21, 39, 45, 764, DateTimeKind.Unspecified).AddTicks(1226), 5 },
                    { 11, new DateTime(1961, 12, 13, 23, 35, 1, 850, DateTimeKind.Unspecified).AddTicks(7727), "Patty.Muller31@gmail.com", "Patty", "Muller", new DateTime(2019, 8, 10, 8, 31, 52, 646, DateTimeKind.Unspecified).AddTicks(7043), 5 },
                    { 9, new DateTime(2004, 8, 8, 3, 7, 16, 700, DateTimeKind.Unspecified).AddTicks(6340), "Meghan_Padberg@hotmail.com", "Meghan", "Padberg", new DateTime(2017, 2, 21, 23, 33, 39, 760, DateTimeKind.Unspecified).AddTicks(1296), 5 },
                    { 3, new DateTime(1969, 11, 20, 4, 15, 21, 121, DateTimeKind.Unspecified).AddTicks(486), "Dolores_Wuckert61@hotmail.com", "Dolores", "Wuckert", new DateTime(2018, 7, 7, 14, 11, 36, 102, DateTimeKind.Unspecified).AddTicks(7190), 5 },
                    { 2, new DateTime(1978, 10, 12, 21, 21, 49, 112, DateTimeKind.Unspecified).AddTicks(3478), "Frances_Champlin60@yahoo.com", "Frances", "Champlin", new DateTime(2019, 2, 3, 4, 32, 20, 433, DateTimeKind.Unspecified).AddTicks(6052), 5 },
                    { 1, new DateTime(1991, 5, 31, 15, 28, 15, 359, DateTimeKind.Unspecified).AddTicks(5690), "Todd.Effertz93@gmail.com", "Todd", "Effertz", new DateTime(2016, 1, 21, 10, 18, 15, 518, DateTimeKind.Unspecified).AddTicks(5839), 5 },
                    { 23, new DateTime(1972, 6, 24, 15, 17, 5, 32, DateTimeKind.Unspecified).AddTicks(2449), "Minnie_Tromp@hotmail.com", "Minnie", "Tromp", new DateTime(2018, 1, 11, 3, 17, 31, 416, DateTimeKind.Unspecified).AddTicks(2298), 4 },
                    { 18, new DateTime(1990, 6, 26, 13, 23, 16, 354, DateTimeKind.Unspecified).AddTicks(2538), "Karla_Krajcik31@hotmail.com", "Karla", "Krajcik", new DateTime(2016, 5, 2, 5, 6, 41, 953, DateTimeKind.Unspecified).AddTicks(2230), 4 },
                    { 16, new DateTime(1981, 6, 6, 18, 25, 50, 843, DateTimeKind.Unspecified).AddTicks(7615), "Jamie_Schmidt77@gmail.com", "Jamie", "Schmidt", new DateTime(2017, 10, 18, 9, 12, 46, 195, DateTimeKind.Unspecified).AddTicks(549), 4 },
                    { 29, new DateTime(2004, 10, 14, 12, 52, 46, 518, DateTimeKind.Unspecified).AddTicks(2716), "Rhonda.Schneider87@hotmail.com", "Rhonda", "Schneider", new DateTime(2018, 11, 4, 16, 22, 7, 643, DateTimeKind.Unspecified).AddTicks(5873), 3 },
                    { 22, new DateTime(1980, 12, 27, 7, 41, 21, 328, DateTimeKind.Unspecified).AddTicks(2828), "Alice17@hotmail.com", "Alice", "Kreiger", new DateTime(2020, 6, 20, 2, 38, 0, 882, DateTimeKind.Unspecified).AddTicks(3153), 3 },
                    { 17, new DateTime(2010, 1, 8, 12, 29, 26, 633, DateTimeKind.Unspecified).AddTicks(56), "Marion6@gmail.com", "Marion", "Bradtke", new DateTime(2019, 2, 5, 18, 49, 13, 758, DateTimeKind.Unspecified).AddTicks(9046), 3 },
                    { 14, new DateTime(1981, 9, 24, 22, 34, 52, 817, DateTimeKind.Unspecified).AddTicks(1984), "Francis.Goyette16@yahoo.com", "Francis", "Goyette", new DateTime(2019, 8, 2, 12, 4, 32, 611, DateTimeKind.Unspecified).AddTicks(2553), 3 },
                    { 10, new DateTime(2008, 3, 23, 13, 52, 43, 559, DateTimeKind.Unspecified).AddTicks(324), "Lynn.Collins67@hotmail.com", "Lynn", "Collins", new DateTime(2016, 11, 4, 3, 23, 40, 691, DateTimeKind.Unspecified).AddTicks(9980), 3 },
                    { 8, new DateTime(1986, 7, 12, 9, 17, 28, 335, DateTimeKind.Unspecified).AddTicks(9507), "Jared95@gmail.com", "Jared", "Schinner", new DateTime(2018, 2, 17, 3, 43, 16, 808, DateTimeKind.Unspecified).AddTicks(6528), 3 },
                    { 5, new DateTime(1960, 5, 29, 22, 27, 30, 692, DateTimeKind.Unspecified).AddTicks(4991), "Marshall.Schmidt40@yahoo.com", "Marshall", "Schmidt", new DateTime(2019, 4, 19, 21, 59, 39, 252, DateTimeKind.Unspecified).AddTicks(8417), 3 },
                    { 28, new DateTime(1998, 8, 14, 18, 2, 29, 848, DateTimeKind.Unspecified).AddTicks(2620), "Catherine.Stiedemann@hotmail.com", "Catherine", "Stiedemann", new DateTime(2016, 7, 7, 6, 47, 50, 458, DateTimeKind.Unspecified).AddTicks(8736), 2 },
                    { 27, new DateTime(1975, 5, 1, 9, 17, 30, 29, DateTimeKind.Unspecified).AddTicks(2830), "Winston_Tromp83@gmail.com", "Winston", "Tromp", new DateTime(2016, 6, 20, 23, 40, 44, 882, DateTimeKind.Unspecified).AddTicks(7485), 2 },
                    { 25, new DateTime(2005, 12, 26, 18, 2, 13, 530, DateTimeKind.Unspecified).AddTicks(3056), "Ramon.Schmidt50@gmail.com", "Ramon", "Schmidt", new DateTime(2019, 6, 17, 19, 46, 28, 255, DateTimeKind.Unspecified).AddTicks(1838), 2 },
                    { 13, new DateTime(1980, 1, 18, 22, 19, 9, 783, DateTimeKind.Unspecified).AddTicks(9303), "Emanuel.Emmerich12@hotmail.com", "Emanuel", "Emmerich", new DateTime(2017, 9, 26, 9, 39, 6, 816, DateTimeKind.Unspecified).AddTicks(9389), 2 },
                    { 12, new DateTime(1967, 7, 30, 16, 5, 49, 871, DateTimeKind.Unspecified).AddTicks(1796), "Luz.McClure46@gmail.com", "Luz", "McClure", new DateTime(2018, 5, 13, 12, 19, 26, 215, DateTimeKind.Unspecified).AddTicks(5418), 2 },
                    { 7, new DateTime(2010, 4, 13, 6, 24, 26, 404, DateTimeKind.Unspecified).AddTicks(6902), "Jamie_Jerde@hotmail.com", "Jamie", "Jerde", new DateTime(2016, 9, 23, 6, 57, 27, 784, DateTimeKind.Unspecified).AddTicks(466), 2 },
                    { 6, new DateTime(1960, 1, 3, 10, 58, 29, 168, DateTimeKind.Unspecified).AddTicks(9815), "Homer93@yahoo.com", "Homer", "Reynolds", new DateTime(2017, 12, 15, 17, 4, 9, 954, DateTimeKind.Unspecified).AddTicks(5905), 2 },
                    { 30, new DateTime(1970, 12, 20, 1, 35, 47, 670, DateTimeKind.Unspecified).AddTicks(541), "Yvonne.Erdman21@gmail.com", "Yvonne", "Erdman", new DateTime(2016, 1, 18, 7, 38, 32, 88, DateTimeKind.Unspecified).AddTicks(2431), 1 },
                    { 26, new DateTime(2002, 1, 11, 3, 33, 59, 317, DateTimeKind.Unspecified).AddTicks(8120), "Loretta.Gutmann@gmail.com", "Loretta", "Gutmann", new DateTime(2019, 12, 15, 21, 3, 44, 290, DateTimeKind.Unspecified).AddTicks(2289), 1 },
                    { 19, new DateTime(1976, 6, 2, 1, 15, 41, 504, DateTimeKind.Unspecified).AddTicks(5178), "Rosie_Metz40@gmail.com", "Rosie", "Metz", new DateTime(2016, 7, 28, 6, 25, 49, 551, DateTimeKind.Unspecified).AddTicks(2519), 1 },
                    { 21, new DateTime(2003, 2, 24, 10, 31, 48, 119, DateTimeKind.Unspecified).AddTicks(1656), "Curtis0@hotmail.com", "Curtis", "McClure", new DateTime(2018, 11, 24, 10, 56, 56, 606, DateTimeKind.Unspecified).AddTicks(7799), 5 },
                    { 24, new DateTime(1982, 4, 3, 12, 52, 37, 128, DateTimeKind.Unspecified).AddTicks(5016), "Ellen.Hudson@hotmail.com", "Ellen", "Hudson", new DateTime(2016, 4, 26, 5, 10, 5, 597, DateTimeKind.Unspecified).AddTicks(8638), 5 }
                });

            migrationBuilder.InsertData(
                table: "Projects",
                columns: new[] { "Id", "AuthorId", "CreatedAt", "Deadline", "Description", "ProjectName", "TeamId" },
                values: new object[,]
                {
                    { 7, 6, new DateTime(2020, 8, 11, 4, 45, 23, 503, DateTimeKind.Unspecified).AddTicks(7094), new DateTime(2022, 10, 13, 4, 23, 24, 277, DateTimeKind.Local).AddTicks(1410), "Minima et qui voluptatem non sint odio harum minima autem pariatur consequatur quae mollitia velit.", "Sequi harum aut aliquam beatae neque.", 5 },
                    { 6, 8, new DateTime(2020, 11, 11, 0, 37, 20, 708, DateTimeKind.Unspecified).AddTicks(9095), new DateTime(2022, 6, 6, 13, 49, 7, 602, DateTimeKind.Local).AddTicks(9636), "Aperiam modi magni necessitatibus voluptate et voluptas harum aut sed et delectus at sapiente amet.", "Eum non totam beatae nemo autem.", 3 },
                    { 9, 10, new DateTime(2018, 4, 8, 0, 13, 25, 522, DateTimeKind.Unspecified).AddTicks(1248), new DateTime(2022, 10, 28, 11, 44, 14, 614, DateTimeKind.Local).AddTicks(5524), "Qui nobis aliquid sunt est et recusandae consequatur earum delectus optio repudiandae deleniti provident modi.", "Suscipit magnam et est qui ullam.", 3 },
                    { 1, 14, new DateTime(2019, 8, 1, 21, 24, 33, 529, DateTimeKind.Unspecified).AddTicks(2391), new DateTime(2022, 1, 1, 19, 49, 6, 517, DateTimeKind.Local).AddTicks(4262), "Voluptatem est impedit iure repellendus dignissimos quo amet voluptatem soluta dolorem quia sit aut eos.", "Dolorem nemo architecto sit amet est.", 4 },
                    { 10, 17, new DateTime(2018, 10, 18, 19, 57, 16, 568, DateTimeKind.Unspecified).AddTicks(8271), new DateTime(2022, 5, 11, 8, 9, 17, 404, DateTimeKind.Local).AddTicks(1547), "Eius architecto in dignissimos a rerum similique eum at sit totam et sapiente sed dolores.", "Et mollitia quam placeat et quas.", 3 },
                    { 5, 29, new DateTime(2019, 1, 6, 10, 0, 45, 298, DateTimeKind.Unspecified).AddTicks(478), new DateTime(2022, 4, 13, 4, 29, 47, 200, DateTimeKind.Local).AddTicks(1209), "Eum sint earum et voluptate architecto et recusandae consequatur voluptas optio sed ipsum odit voluptates.", "Soluta repellendus officiis omnis reiciendis illo.", 5 },
                    { 8, 18, new DateTime(2018, 5, 3, 2, 13, 11, 439, DateTimeKind.Unspecified).AddTicks(5384), new DateTime(2022, 3, 24, 21, 15, 52, 111, DateTimeKind.Local).AddTicks(3368), "Natus eius cupiditate sequi maxime voluptatem ut illo est consequuntur molestias aliquam atque ducimus quis.", "Maiores omnis est maiores omnis non.", 3 },
                    { 4, 11, new DateTime(2017, 3, 16, 12, 47, 5, 619, DateTimeKind.Unspecified).AddTicks(2153), new DateTime(2021, 10, 2, 2, 8, 45, 692, DateTimeKind.Local).AddTicks(7440), "Corporis qui laborum doloremque officia odio voluptates molestias quis voluptatem sed eveniet aliquam qui at.", "Perspiciatis vitae officia sed similique consectetur.", 5 },
                    { 3, 15, new DateTime(2019, 11, 19, 21, 42, 21, 613, DateTimeKind.Unspecified).AddTicks(1667), new DateTime(2022, 2, 19, 15, 28, 21, 973, DateTimeKind.Local).AddTicks(8732), "Distinctio et quas velit ratione sunt voluptatem velit optio aliquid et aut quia tempora rerum.", "Deleniti aut doloribus aut velit quos.", 1 },
                    { 2, 24, new DateTime(2017, 10, 15, 10, 4, 18, 662, DateTimeKind.Unspecified).AddTicks(7727), new DateTime(2022, 9, 10, 5, 16, 55, 925, DateTimeKind.Local).AddTicks(5028), "Et perferendis nihil et et id in qui perspiciatis earum ipsam magnam inventore laboriosam aut.", "Maiores et ipsum ut molestias eligendi.", 5 }
                });

            migrationBuilder.InsertData(
                table: "Tasks",
                columns: new[] { "Id", "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[,]
                {
                    { 4, new DateTime(2019, 4, 27, 23, 53, 31, 652, DateTimeKind.Unspecified).AddTicks(3979), "modi", null, "Eaque dicta iste sequi praesentium.", 6, 7, 2 },
                    { 58, new DateTime(2019, 9, 19, 10, 19, 11, 738, DateTimeKind.Unspecified).AddTicks(9590), "Eveniet sed quia a facere vitae iusto explicabo eligendi sit. Voluptatibus ut modi et eos voluptates. Asperiores sed quis suscipit. Itaque dolorem commodi et eaque. Eos nostrum ex at magnam. Et non repellendus laborum.", new DateTime(2020, 3, 21, 15, 14, 56, 626, DateTimeKind.Unspecified).AddTicks(8252), "Quidem sed non dolor sint.", 22, 10, 2 },
                    { 59, new DateTime(2019, 1, 19, 18, 31, 1, 12, DateTimeKind.Unspecified).AddTicks(8296), "Quo facilis error eum ut dolores iure numquam qui omnis. Ipsa quasi magni explicabo explicabo deserunt molestiae consequuntur culpa. Incidunt officia expedita minus nemo. Eligendi qui dolorem molestiae. Error praesentium eos est natus qui reiciendis est modi nihil.", new DateTime(2019, 7, 23, 13, 10, 2, 369, DateTimeKind.Unspecified).AddTicks(9625), "In libero esse officiis reprehenderit.", 26, 10, 2 },
                    { 68, new DateTime(2020, 1, 15, 23, 31, 3, 583, DateTimeKind.Unspecified).AddTicks(2257), "Iusto voluptatem cum.\nBlanditiis quisquam eveniet hic aut laborum temporibus.\nIusto delectus laudantium modi aut ducimus.\nSimilique et quaerat ut aut numquam debitis.\nTenetur iusto qui exercitationem.\nQuas inventore voluptatibus omnis.", new DateTime(2019, 3, 15, 6, 44, 33, 711, DateTimeKind.Unspecified).AddTicks(6434), "Atque rerum quidem minus eveniet.", 19, 10, 1 },
                    { 23, new DateTime(2020, 4, 11, 23, 35, 53, 371, DateTimeKind.Unspecified).AddTicks(3945), "excepturi", null, "Iusto voluptas esse minima repudiandae.", 26, 5, 2 },
                    { 26, new DateTime(2020, 2, 18, 6, 32, 32, 836, DateTimeKind.Unspecified).AddTicks(1698), "Natus voluptas corporis aperiam tempora vel magnam et.\nMagni sed est id illum saepe.\nRepudiandae et ipsum et officiis aut atque officiis.\nUt deserunt aspernatur voluptatem excepturi ipsa facere non doloribus sit.\nSequi molestiae repudiandae et reiciendis ratione laboriosam quia.\nVelit est dolor inventore repellendus unde.", new DateTime(2018, 12, 24, 12, 0, 26, 929, DateTimeKind.Unspecified).AddTicks(4770), "Asperiores tenetur ducimus laudantium atque.", 4, 5, 3 },
                    { 32, new DateTime(2020, 7, 15, 4, 44, 46, 948, DateTimeKind.Unspecified).AddTicks(3252), "In odit et earum et nihil doloremque nisi tempore occaecati. Quasi culpa incidunt quidem repellat. Omnis quos ullam enim deleniti saepe corrupti. Omnis deleniti aliquid et. Aut aut cupiditate rerum dolores.", null, "Perferendis occaecati quia totam quis.", 5, 5, 3 },
                    { 65, new DateTime(2018, 4, 10, 19, 5, 40, 278, DateTimeKind.Unspecified).AddTicks(3982), "Hic accusantium repellat est esse aut quaerat sed. Eveniet totam dolorum iste. Sit excepturi consequatur corporis eligendi in qui omnis quis reiciendis. Laborum molestiae eum et est laboriosam. Eaque doloremque debitis magnam. Saepe vel numquam non ut.", null, "Harum enim fugiat nesciunt unde.", 28, 5, 3 },
                    { 66, new DateTime(2017, 2, 1, 22, 5, 0, 723, DateTimeKind.Unspecified).AddTicks(354), "laudantium", null, "Cum reiciendis quia aliquid et.", 23, 5, 3 },
                    { 12, new DateTime(2019, 2, 26, 19, 51, 30, 131, DateTimeKind.Unspecified).AddTicks(7602), "Quibusdam nam numquam. Sed qui praesentium voluptas vitae hic quos voluptas sunt. Ab sit quod neque. Reprehenderit corrupti sint ex voluptatem eos ducimus nostrum. Ut repudiandae sit ratione laborum.", new DateTime(2019, 8, 17, 13, 36, 38, 289, DateTimeKind.Unspecified).AddTicks(4892), "Quo vel officia quae quam.", 8, 8, 1 },
                    { 15, new DateTime(2017, 4, 5, 3, 15, 15, 437, DateTimeKind.Unspecified).AddTicks(5667), "Voluptatibus et incidunt recusandae autem veniam et amet porro ut. Iste quia exercitationem sunt eligendi et quod. Praesentium provident delectus mollitia. Qui ratione eveniet eaque beatae qui odit. Eum est facilis est.", new DateTime(2018, 4, 8, 16, 42, 3, 173, DateTimeKind.Unspecified).AddTicks(4677), "Quam voluptatem eos molestiae vero.", 13, 8, 3 },
                    { 19, new DateTime(2020, 6, 18, 10, 27, 36, 935, DateTimeKind.Unspecified).AddTicks(2534), "Assumenda ullam reiciendis sed consectetur.\nIllo minus aliquid est.\nOccaecati aliquam exercitationem.", null, "Similique dolores voluptatum atque cum.", 15, 8, 0 },
                    { 33, new DateTime(2018, 6, 6, 10, 44, 24, 264, DateTimeKind.Unspecified).AddTicks(6925), "Aut dolor recusandae cupiditate enim iure facilis et non.\nTemporibus corporis tempora soluta nulla nisi.\nDolor perspiciatis eligendi aspernatur impedit omnis eum.\nAd labore voluptas id illum excepturi officia repellat optio totam.\nQui atque fugiat eos et.", new DateTime(2021, 6, 24, 2, 12, 41, 281, DateTimeKind.Unspecified).AddTicks(495), "Inventore unde officiis beatae aut.", 2, 8, 0 },
                    { 43, new DateTime(2019, 12, 17, 13, 48, 15, 409, DateTimeKind.Unspecified).AddTicks(5312), "Id deleniti placeat dolorum doloribus expedita libero ut dicta esse.", new DateTime(2020, 7, 28, 4, 55, 56, 816, DateTimeKind.Unspecified).AddTicks(7188), "Molestiae dignissimos aliquid maxime est.", 15, 8, 2 },
                    { 62, new DateTime(2020, 1, 22, 0, 6, 45, 684, DateTimeKind.Unspecified).AddTicks(4624), "Quam sint expedita omnis.", new DateTime(2020, 4, 21, 22, 35, 37, 264, DateTimeKind.Unspecified).AddTicks(8196), "Quod aut sed magni aut.", 2, 8, 2 },
                    { 63, new DateTime(2020, 4, 18, 21, 36, 29, 887, DateTimeKind.Unspecified).AddTicks(8162), "Repellendus excepturi occaecati.", null, "Nisi beatae aut aperiam tenetur.", 19, 8, 1 },
                    { 70, new DateTime(2018, 5, 21, 11, 48, 42, 457, DateTimeKind.Unspecified).AddTicks(8087), "Dolore non aut ut id error eligendi fuga. Autem expedita praesentium deserunt. Laboriosam aut nulla velit dolores eaque voluptatum voluptatum quibusdam ut. Debitis ex nihil omnis. Veniam minus necessitatibus id cum dolorem saepe. Autem blanditiis voluptatibus eius.", null, "Dolores id quae labore tempore.", 6, 8, 3 },
                    { 1, new DateTime(2018, 4, 23, 0, 47, 26, 165, DateTimeKind.Unspecified).AddTicks(9758), "Id sit ut nihil enim itaque minus officia reprehenderit iste.", null, "Officia similique at ipsa commodi.", 30, 4, 2 },
                    { 34, new DateTime(2019, 3, 16, 20, 9, 38, 627, DateTimeKind.Unspecified).AddTicks(33), "quasi", new DateTime(2021, 6, 30, 5, 47, 5, 716, DateTimeKind.Unspecified).AddTicks(138), "Vel cumque provident similique quibusdam.", 19, 2, 2 },
                    { 11, new DateTime(2018, 8, 11, 18, 28, 49, 825, DateTimeKind.Unspecified).AddTicks(881), "Sint possimus voluptatem qui repellendus rerum expedita.", null, "Dolorem a iusto voluptatem numquam.", 5, 2, 3 },
                    { 52, new DateTime(2018, 12, 29, 6, 49, 46, 581, DateTimeKind.Unspecified).AddTicks(5188), "dolores", null, "Occaecati quaerat alias ab velit.", 7, 3, 0 },
                    { 50, new DateTime(2018, 7, 2, 10, 14, 48, 405, DateTimeKind.Unspecified).AddTicks(824), "voluptates", new DateTime(2020, 3, 30, 6, 56, 43, 74, DateTimeKind.Unspecified).AddTicks(6438), "Sed hic est tempora provident.", 4, 3, 0 },
                    { 40, new DateTime(2020, 11, 23, 22, 2, 49, 370, DateTimeKind.Unspecified).AddTicks(8610), "Labore doloribus praesentium enim est.", new DateTime(2018, 4, 16, 20, 55, 36, 910, DateTimeKind.Unspecified).AddTicks(7103), "Est voluptates rem facere ab.", 24, 3, 3 },
                    { 35, new DateTime(2020, 10, 17, 11, 2, 35, 858, DateTimeKind.Unspecified).AddTicks(7721), "nemo", null, "Ratione debitis voluptatem qui et.", 16, 3, 2 },
                    { 55, new DateTime(2019, 10, 27, 4, 31, 6, 104, DateTimeKind.Unspecified).AddTicks(1753), "Quasi nobis eos exercitationem velit et et natus vitae quos.", null, "Id minus eligendi et iure.", 23, 10, 1 },
                    { 30, new DateTime(2018, 12, 19, 7, 59, 22, 384, DateTimeKind.Unspecified).AddTicks(4996), "Voluptas libero nemo dolorum impedit quam.", new DateTime(2020, 9, 24, 17, 19, 8, 859, DateTimeKind.Unspecified).AddTicks(8414), "Ut voluptatem totam vel voluptatem.", 1, 3, 1 },
                    { 18, new DateTime(2019, 12, 3, 11, 24, 38, 70, DateTimeKind.Unspecified).AddTicks(7118), "Ex quisquam et fugiat velit rem eum excepturi saepe temporibus.\nOdio et voluptatibus culpa excepturi expedita.\nLaborum officia voluptatum quos ducimus nulla.", new DateTime(2018, 11, 24, 14, 35, 10, 802, DateTimeKind.Unspecified).AddTicks(6090), "Harum libero doloremque voluptates voluptas.", 1, 3, 0 },
                    { 10, new DateTime(2017, 1, 8, 16, 12, 49, 608, DateTimeKind.Unspecified).AddTicks(4324), "Ut omnis consequatur placeat iusto laboriosam ratione deleniti omnis.", null, "Officia ipsa iure excepturi perspiciatis.", 18, 3, 1 },
                    { 48, new DateTime(2017, 2, 18, 9, 7, 48, 852, DateTimeKind.Unspecified).AddTicks(5115), "Aut ut esse placeat minus.", null, "Reprehenderit impedit est nulla enim.", 14, 4, 0 },
                    { 44, new DateTime(2020, 5, 14, 15, 19, 47, 352, DateTimeKind.Unspecified).AddTicks(5072), "Natus voluptatem voluptate temporibus ipsam nobis ipsam.", new DateTime(2018, 2, 13, 5, 27, 43, 884, DateTimeKind.Unspecified).AddTicks(4316), "Et ducimus molestiae impedit alias.", 2, 4, 3 },
                    { 25, new DateTime(2017, 7, 16, 2, 52, 15, 108, DateTimeKind.Unspecified).AddTicks(4206), "aut", null, "Totam blanditiis praesentium eos totam.", 16, 4, 3 },
                    { 7, new DateTime(2019, 4, 6, 18, 24, 22, 567, DateTimeKind.Unspecified).AddTicks(2336), "perferendis", new DateTime(2018, 6, 23, 13, 44, 22, 64, DateTimeKind.Unspecified).AddTicks(3060), "Dicta non voluptatem molestias vero.", 5, 4, 2 },
                    { 27, new DateTime(2020, 11, 4, 8, 23, 3, 667, DateTimeKind.Unspecified).AddTicks(3564), "Consequatur excepturi et.\nConsectetur molestias corrupti nihil sint qui.\nLaudantium ad totam dolores assumenda velit eaque voluptatum.\nDolor aliquid aut quia sunt nihil est ullam tempora et.\nPerspiciatis eum fugit ipsa ut sequi eligendi accusamus ad.\nNesciunt aut nostrum.", new DateTime(2021, 3, 21, 20, 8, 23, 126, DateTimeKind.Unspecified).AddTicks(6714), "Molestiae consequuntur est modi sequi.", 1, 3, 0 },
                    { 53, new DateTime(2020, 5, 12, 10, 32, 26, 464, DateTimeKind.Unspecified).AddTicks(3208), "In aut et ut ea ad possimus non eos.\nCupiditate quidem magni fugiat.\nEveniet voluptates aperiam.", null, "Aperiam rerum sapiente totam fugit.", 27, 10, 0 },
                    { 46, new DateTime(2017, 4, 5, 1, 49, 52, 354, DateTimeKind.Unspecified).AddTicks(6703), "Eius exercitationem autem. Ea corporis quo odit. Sit aut et deserunt nesciunt.", new DateTime(2020, 12, 25, 4, 37, 26, 977, DateTimeKind.Unspecified).AddTicks(5101), "Sed tempore facilis explicabo voluptates.", 12, 10, 1 },
                    { 41, new DateTime(2020, 1, 6, 7, 42, 18, 796, DateTimeKind.Unspecified).AddTicks(1264), "nam", null, "Consequatur earum consectetur hic labore.", 17, 10, 2 },
                    { 22, new DateTime(2018, 9, 2, 17, 44, 1, 443, DateTimeKind.Unspecified).AddTicks(1288), "Quos velit hic aut veritatis laudantium.\nAt facere ut est quas perferendis voluptatem voluptate.\nAspernatur tempore nobis.", new DateTime(2021, 3, 2, 14, 7, 34, 336, DateTimeKind.Unspecified).AddTicks(347), "Vitae praesentium tempora quidem hic.", 8, 9, 2 },
                    { 51, new DateTime(2018, 11, 28, 7, 1, 39, 878, DateTimeKind.Unspecified).AddTicks(2335), "iste", null, "Iusto aperiam rem omnis sint.", 10, 6, 3 },
                    { 42, new DateTime(2020, 9, 5, 18, 37, 43, 204, DateTimeKind.Unspecified).AddTicks(3316), "ut", new DateTime(2018, 7, 29, 5, 51, 30, 918, DateTimeKind.Unspecified).AddTicks(6958), "Sint in architecto aliquid et.", 25, 6, 1 },
                    { 31, new DateTime(2018, 1, 12, 19, 42, 45, 268, DateTimeKind.Unspecified).AddTicks(6643), "Numquam voluptas provident aperiam nemo sunt hic molestiae sunt.", new DateTime(2019, 3, 15, 16, 42, 0, 782, DateTimeKind.Unspecified).AddTicks(8778), "Aut dolores accusamus cum quibusdam.", 18, 6, 3 },
                    { 24, new DateTime(2017, 6, 26, 12, 10, 59, 99, DateTimeKind.Unspecified).AddTicks(4026), "Veritatis amet inventore illo nesciunt ut ad. Provident qui et asperiores delectus reiciendis fuga cumque ipsam. Quo labore numquam earum perspiciatis ipsum officia. Beatae eos in corporis in odio expedita dignissimos. Dolore nihil commodi quia autem possimus rerum doloremque rerum. Dolorem in ipsa repudiandae qui ab quo.", new DateTime(2021, 2, 12, 1, 7, 54, 362, DateTimeKind.Unspecified).AddTicks(4851), "Voluptatem sed veniam consequatur veniam.", 10, 6, 3 },
                    { 5, new DateTime(2019, 5, 26, 18, 58, 50, 738, DateTimeKind.Unspecified).AddTicks(5554), "Cupiditate dolores autem.\nQuis maxime iure deleniti consequatur tempora vel voluptatibus rem doloremque.\nEt voluptatem fugit similique saepe.\nEt iusto et dignissimos.\nDolor et deserunt ad eos quia porro nihil.", new DateTime(2020, 7, 3, 13, 22, 27, 531, DateTimeKind.Unspecified).AddTicks(4448), "Non ut iste et libero.", 22, 6, 1 }
                });

            migrationBuilder.InsertData(
                table: "Tasks",
                columns: new[] { "Id", "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[,]
                {
                    { 38, new DateTime(2019, 2, 26, 23, 18, 22, 480, DateTimeKind.Unspecified).AddTicks(6145), "Illo cupiditate eveniet earum.\nUt deleniti ut recusandae consectetur.\nQuibusdam molestias at et sit eveniet mollitia.\nOmnis iusto debitis libero et corporis et enim repellendus quo.\nPorro sed ex dolores incidunt ut.", null, "Natus voluptatem deleniti fugit cumque.", 14, 9, 0 },
                    { 3, new DateTime(2018, 6, 29, 21, 34, 21, 419, DateTimeKind.Unspecified).AddTicks(5530), "voluptatem", new DateTime(2021, 4, 19, 4, 49, 12, 288, DateTimeKind.Unspecified).AddTicks(1097), "Eaque doloribus accusantium eaque iure.", 27, 6, 2 },
                    { 61, new DateTime(2020, 3, 25, 7, 9, 5, 521, DateTimeKind.Unspecified).AddTicks(791), "Asperiores atque sed assumenda rerum voluptatum ea.", null, "Optio voluptas incidunt pariatur quo.", 20, 7, 3 },
                    { 56, new DateTime(2017, 1, 24, 1, 34, 28, 78, DateTimeKind.Unspecified).AddTicks(4118), "et", new DateTime(2019, 1, 10, 22, 46, 33, 515, DateTimeKind.Unspecified).AddTicks(6960), "Minus enim perspiciatis aut magni.", 16, 7, 1 },
                    { 29, new DateTime(2017, 9, 4, 4, 47, 3, 701, DateTimeKind.Unspecified).AddTicks(2750), "molestiae", null, "Deleniti dicta asperiores sit beatae.", 22, 7, 2 },
                    { 14, new DateTime(2020, 6, 15, 0, 37, 48, 676, DateTimeKind.Unspecified).AddTicks(7320), "Et deleniti magnam cumque mollitia et.\nRerum odit id temporibus voluptate temporibus et.\nEst omnis et qui soluta quas earum officia.\nAutem et nostrum consequatur nihil ipsam voluptatem.\nEius quasi sapiente sunt illo.\nEt nemo doloribus quidem sed ut quia.", new DateTime(2018, 8, 7, 10, 48, 8, 299, DateTimeKind.Unspecified).AddTicks(4786), "Adipisci non ipsum recusandae nisi.", 21, 7, 1 },
                    { 9, new DateTime(2019, 10, 25, 1, 23, 51, 124, DateTimeKind.Unspecified).AddTicks(8758), "ad", new DateTime(2019, 11, 19, 18, 12, 12, 667, DateTimeKind.Unspecified).AddTicks(8959), "Consequatur illo dolorem enim rerum.", 2, 7, 0 },
                    { 8, new DateTime(2019, 4, 9, 2, 37, 55, 657, DateTimeKind.Unspecified).AddTicks(1173), "alias", new DateTime(2020, 9, 26, 7, 1, 40, 791, DateTimeKind.Unspecified).AddTicks(4571), "Reprehenderit corporis quis occaecati atque.", 29, 7, 2 },
                    { 67, new DateTime(2019, 9, 7, 20, 11, 5, 419, DateTimeKind.Unspecified).AddTicks(1296), "Cupiditate beatae vitae velit repudiandae et voluptatum.\nDolores soluta voluptas quasi magni consectetur facere ea dolor.\nAspernatur quae in beatae deleniti excepturi.", new DateTime(2020, 3, 1, 9, 17, 21, 899, DateTimeKind.Unspecified).AddTicks(9174), "Aut perspiciatis debitis et eum.", 18, 7, 1 },
                    { 39, new DateTime(2017, 6, 20, 14, 6, 47, 595, DateTimeKind.Unspecified).AddTicks(9353), "Voluptatem numquam odit et aut quod dolor at.\nQuidem ducimus beatae eligendi quidem sed voluptatum asperiores dignissimos.\nFuga corporis quia.\nSequi iure possimus.\nPerspiciatis ut minima.", new DateTime(2018, 8, 22, 9, 2, 9, 367, DateTimeKind.Unspecified).AddTicks(8261), "Ea suscipit assumenda repellendus quam.", 17, 2, 3 },
                    { 45, new DateTime(2019, 8, 28, 13, 41, 8, 1, DateTimeKind.Unspecified).AddTicks(8312), "Quaerat sit corporis nihil.\nImpedit cumque ullam sit dolor voluptates ad ut consectetur animi.", new DateTime(2019, 11, 5, 12, 0, 9, 449, DateTimeKind.Unspecified).AddTicks(9591), "Dolorem qui commodi quia esse.", 11, 9, 2 },
                    { 2, new DateTime(2018, 6, 20, 7, 5, 45, 768, DateTimeKind.Unspecified).AddTicks(1089), "Ut suscipit voluptatem debitis.", null, "Provident et sint possimus voluptatem.", 28, 1, 1 },
                    { 37, new DateTime(2017, 11, 12, 22, 59, 0, 162, DateTimeKind.Unspecified).AddTicks(2353), "Saepe quia sed sequi ad. Dolorem dolor qui ut ducimus est. Autem dolor eos cupiditate aliquid porro quis. Dolore qui ratione harum autem occaecati sint eum odit. Sint quam dicta repellat. Quis officiis voluptatem illum eligendi necessitatibus corrupti impedit.", null, "Aut exercitationem optio nulla fugiat.", 10, 10, 1 },
                    { 28, new DateTime(2020, 2, 17, 16, 5, 36, 933, DateTimeKind.Unspecified).AddTicks(3701), "voluptate", null, "Consequatur sed consequuntur asperiores repudiandae.", 23, 10, 1 },
                    { 20, new DateTime(2020, 1, 23, 11, 21, 15, 638, DateTimeKind.Unspecified).AddTicks(2552), "Maiores delectus iusto corporis.\nSed aliquid cumque ipsam est ea corporis dolore autem.\nNon quis rerum eum alias.\nEt voluptas asperiores consequatur dignissimos.\nTotam quidem pariatur debitis molestias.", new DateTime(2019, 3, 19, 22, 7, 27, 799, DateTimeKind.Unspecified).AddTicks(6557), "Inventore fugiat magni qui aspernatur.", 2, 10, 2 },
                    { 17, new DateTime(2017, 9, 9, 21, 36, 24, 409, DateTimeKind.Unspecified).AddTicks(6725), "Odit in iusto eum quia iste.\nIn recusandae dolorem sit quia assumenda qui aperiam.\nNumquam eos dolor est aut harum.", null, "Autem perferendis voluptatem qui autem.", 17, 10, 0 },
                    { 13, new DateTime(2019, 9, 7, 15, 19, 55, 351, DateTimeKind.Unspecified).AddTicks(4177), "Quas omnis similique.\nAliquam modi numquam qui minima.", null, "Illo corrupti saepe debitis dolore.", 28, 10, 1 },
                    { 6, new DateTime(2018, 7, 27, 9, 4, 1, 23, DateTimeKind.Unspecified).AddTicks(4916), "Id quos aut omnis.\nOmnis aut accusantium veritatis.\nExcepturi et error qui rerum provident commodi.\nUt assumenda laborum possimus.\nQuisquam magnam at et qui.\nInventore doloribus nulla nihil repellat rerum aut dolorem enim.", new DateTime(2018, 6, 23, 17, 21, 14, 252, DateTimeKind.Unspecified).AddTicks(463), "Et maxime quisquam vel consequatur.", 25, 10, 0 },
                    { 57, new DateTime(2019, 3, 27, 11, 9, 12, 365, DateTimeKind.Unspecified).AddTicks(9759), "Tempore facere non dolore perferendis cupiditate et. Porro est vitae nihil quia repellat eum. Quas sequi nulla rerum non dolore. Sit accusantium assumenda iure quod commodi quisquam ea autem.", null, "Quia laboriosam magni officia officia.", 2, 9, 1 },
                    { 64, new DateTime(2018, 6, 26, 18, 9, 24, 183, DateTimeKind.Unspecified).AddTicks(4116), "Deserunt veritatis eligendi suscipit recusandae rerum.\nEveniet quo eum.\nEnim qui quibusdam amet temporibus.", new DateTime(2021, 6, 5, 9, 50, 38, 677, DateTimeKind.Unspecified).AddTicks(6734), "Aut debitis amet vitae in.", 11, 1, 1 },
                    { 54, new DateTime(2019, 6, 2, 22, 35, 45, 404, DateTimeKind.Unspecified).AddTicks(1994), "dicta", new DateTime(2019, 6, 8, 1, 48, 0, 65, DateTimeKind.Unspecified).AddTicks(480), "Tempora sed quae qui officiis.", 1, 1, 2 },
                    { 49, new DateTime(2018, 1, 1, 22, 31, 7, 770, DateTimeKind.Unspecified).AddTicks(6217), "Qui placeat sunt ullam.", new DateTime(2018, 1, 15, 6, 21, 32, 721, DateTimeKind.Unspecified).AddTicks(1238), "Quia fugit vel adipisci quibusdam.", 9, 1, 3 },
                    { 47, new DateTime(2017, 8, 15, 1, 37, 36, 293, DateTimeKind.Unspecified).AddTicks(3636), "Distinctio sit quasi et et numquam sapiente.\nProvident quia laborum rerum omnis deleniti.", new DateTime(2018, 2, 26, 8, 10, 10, 442, DateTimeKind.Unspecified).AddTicks(3348), "Dolor laboriosam inventore sapiente ea.", 21, 1, 2 },
                    { 36, new DateTime(2019, 11, 14, 18, 3, 28, 745, DateTimeKind.Unspecified).AddTicks(7900), "Exercitationem minima eos.", new DateTime(2021, 4, 2, 10, 56, 32, 786, DateTimeKind.Unspecified).AddTicks(4830), "Ut voluptate qui necessitatibus asperiores.", 8, 1, 0 },
                    { 21, new DateTime(2019, 4, 6, 0, 37, 47, 881, DateTimeKind.Unspecified).AddTicks(3632), "dolores", null, "Incidunt ipsam ut mollitia placeat.", 9, 1, 2 },
                    { 16, new DateTime(2019, 8, 19, 0, 59, 19, 789, DateTimeKind.Unspecified).AddTicks(6250), "incidunt", null, "Recusandae sequi quis consequatur delectus.", 4, 1, 1 },
                    { 60, new DateTime(2017, 5, 24, 9, 0, 9, 270, DateTimeKind.Unspecified).AddTicks(9767), "ut", new DateTime(2021, 2, 6, 9, 4, 22, 260, DateTimeKind.Unspecified).AddTicks(3036), "In voluptatem veniam totam maxime.", 28, 1, 1 },
                    { 69, new DateTime(2020, 5, 8, 16, 40, 2, 524, DateTimeKind.Unspecified).AddTicks(3398), "Quisquam ducimus repellat sit eius dolor.\nAccusamus autem in occaecati earum sint quo voluptatem harum ut.\nVoluptatibus nobis at doloremque dolorem quo.\nError porro vel molestiae eaque optio cum provident sit tempore.", new DateTime(2020, 10, 27, 19, 36, 3, 974, DateTimeKind.Unspecified).AddTicks(166), "Perspiciatis delectus aut dolor sit.", 6, 2, 3 }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 8);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 9);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 10);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 11);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 12);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 13);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 14);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 15);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 16);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 17);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 18);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 19);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 20);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 21);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 22);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 23);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 24);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 25);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 26);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 27);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 28);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 29);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 30);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 31);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 32);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 33);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 34);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 35);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 36);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 37);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 38);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 39);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 40);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 41);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 42);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 43);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 44);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 45);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 46);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 47);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 48);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 49);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 50);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 51);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 52);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 53);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 54);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 55);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 56);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 57);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 58);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 59);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 60);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 61);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 62);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 63);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 64);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 65);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 66);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 67);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 68);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 69);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 70);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 8);

            migrationBuilder.DeleteData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 9);

            migrationBuilder.DeleteData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 10);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 9);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 12);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 13);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 16);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 19);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 20);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 21);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 22);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 23);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 25);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 26);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 27);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 28);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 30);

            migrationBuilder.DeleteData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 8);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 10);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 11);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 14);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 15);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 17);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 18);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 24);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 29);

            migrationBuilder.DeleteData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 5);
        }
    }
}
