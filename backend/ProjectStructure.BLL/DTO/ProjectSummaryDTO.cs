﻿
namespace ProjectStructure.BLL.DTO
{
    public sealed class ProjectSummaryDTO
    {
        public ProjectDTO Project { get; set; }
        public TaskDTO LongestTaskByDescription { get; set; }
        public TaskDTO ShortestTaskByName { get; set; }
        public int TeamMemberAmount { get; set; }
    }
}
