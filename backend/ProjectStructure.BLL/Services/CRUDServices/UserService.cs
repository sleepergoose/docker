﻿using System;
using AutoMapper;
using System.Threading.Tasks;
using ProjectStructure.BLL.DTO;
using System.Collections.Generic;
using ProjectStructure.DAL.Context;
using Microsoft.EntityFrameworkCore;
using ProjectStructure.BLL.Services.Abstract;
using ProjectStructure.BLL.Services.Interfaces;

namespace ProjectStructure.BLL.Services
{
    public class UserService : BaseService, IService<UserDTO>
    {
        public UserService(ApplicationContext context, IMapper mapper)
            : base(context, mapper)
        { }


        public async Task<IEnumerable<UserDTO>> GetAllEntitiesAsync()
        {
            var users = await _context.Users
                .AsNoTracking()
                .ToListAsync();

            return _mapper.Map<IEnumerable<UserDTO>>(users);
        }


        public async Task<UserDTO> GetEntityAsync(int id)
        {
            var user = await _context.Users
                .AsNoTracking()
                .FirstOrDefaultAsync(u => u.Id == id);

            return _mapper.Map<UserDTO>(user);
        }


        public async Task<UserDTO> AddEntityAsync(UserDTO userDTO)
        {
            if (userDTO == null)
                throw new ArgumentNullException("Argument cannot be null");

            try
            {
                userDTO.Id = 0; // This excludes conflicts with database 

                var user = _mapper.Map<DAL.Entities.User>(userDTO);

                await _context.Users.AddAsync(user);

                await _context.SaveChangesAsync();

                var createdUser = await GetEntityAsync(user.Id);

                return _mapper.Map<UserDTO>(createdUser);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        public async Task<UserDTO> UpdateEntityAsync(UserDTO userDTO)
        {
            if (userDTO == null)
                throw new ArgumentNullException("Argument cannot be null");

            try
            {
                var user = _mapper.Map<DAL.Entities.User>(userDTO);

                _context.Entry(user).State = EntityState.Modified;

                await _context.SaveChangesAsync();

                var updatedUser = await GetEntityAsync(user.Id);

                return _mapper.Map<UserDTO>(updatedUser);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        public async Task<int> DeleteEntityAsync(int id)
        {
            try
            {
                var deletedUser = await _context.Users.FirstOrDefaultAsync(u => u.Id == id);

                _context.Users.Remove(deletedUser);

                await _context.SaveChangesAsync();

                return id;
            }
            catch (ArgumentNullException ex)
            {
                throw new ArgumentNullException(ex.Message);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}
